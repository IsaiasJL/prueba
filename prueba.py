#############################################################################################################################################################
#################################   ESTE CODIGO FUE RELIZADO EN GOOGLE COLAB DESDE EL ENLACE PROPORCIONADO PARA SU ANALISIS   ###############################
#############################################################################################################################################################
import requests
import datetime
response = requests.get('https://api.stackexchange.com/2.2/search?order=desc&sort=activity&intitle=perl&site=stackoverflow')
#print(response.json())
f = response.json()
#############################################################################################################################################################
########################################   2. Obtener el número de respuestas contestadas y no contestadas  #################################################
#############################################################################################################################################################
#iniciamos 2 variables para gurdar el numero de preguntas contestadas y no contestadas
contestadas = 0
nocontestadas = 0
#iniciamos un ciclo para recorrer la lista items
for contador in range(0,30):
 is_answered = (f["items"][contador]["is_answered"]) 
#comparamos si is_answered es TRUE
 r2 = is_answered == True
#si es TRUE sumamos 1 al contador contestadas
 if r2==True:
   contestadas = contestadas+1 
#si es FALSE sumamos 1 nocontestadas     
 if r2==False:
   nocontestadas = nocontestadas+1
#imprimimos las 2 variables   
print(contestadas,"Preguntas fueron contestadas")
print(nocontestadas,"Preguntas no fueron contestadas")
#############################################################################################################################################################
####################################################   3. Obtener la respuesta con mayor owners  ############################################################
#####################   ESTA PREGUNTA NO ME RESULTO MUY CLARA, PERO INTERPRETE QUE SE REFERIA A LA REPUTACION DEL OWNER   ###################################
#############################################################################################################################################################
#iniciamos una variable en 0
mayorrep = 0
#iniciamos un ciclo para recorrer la lista items
for contador in range(0,30):
 reputation = (f["items"][contador]["owner"]["reputation"]) 
 if reputation>mayorrep:
   mayorrep=reputation
   contador1=contador
print()   
#imprimimos el contaodr con la menor reputacion
print("la respuesta con el owner mayor reputacionn es la :",contador1,"con:",mayorrep,"de reputacion")
#############################################################################################################################################################
###############################################   4. Obtener la respuesta con menor número de vistas  #######################################################
#############################################################################################################################################################
#iniciamos una variable en 0
mayor = 0
#iniciamos un ciclo para recorrer la lista items
for contador in range(0,30):
 view_count = (f["items"][contador]["view_count"]) 
#iniciamos una condicion en donde si view_count es mayor a la variable mayor view_count se asigna el nuevo view_count a la bariable mayor hasta que recorra
#toda la lista y obtener en numero de vistas mayor 
 if view_count>mayor:
   mayor=view_count
#iniciamos la variable menor igual a la variable mayor
menor = mayor
#iniciamos un ciclo para recorrer la lista items
for contador in range(0,30):
  view_count = (f["items"][contador]["view_count"])
#iniciamos una condicion en donde si view_count es menor a la variable mayor se asigna en nuevo view_count a la variable menor hasta que rrecorra toda la 
#lista y obtener el numero de visitas menor 
  if view_count<menor:
    menor=view_count
#este contador sirve para obtener el nuemro de pregunta de la lista
    contador2=contador
print()  
#imprimimos el contaodr con el numero menor de visitas  
print("la respuesta con menor numero de visitas es la:",contador2,"con:",menor,"visitas")
#############################################################################################################################################################
###################################################   5. Obtener la respuesta más vieja y más actual  #######################################################
#############################################################################################################################################################
#iniciamos la variable actual en 0
actual = 0
#iniciamos un ciclo para recorrer la lista items
for contador in range(0,30):
 creation_date = (f["items"][contador]["creation_date"])
#iniciamos una condicion, si creation_date es mayor a la variable actual entonces le asignamos creation_date a la variable actual
 if creation_date>actual:
   actual=creation_date
#este contador sirve para obtener el nuemro de pregunta de la lista
   contador1=contador
#iniciamos la variable vieja igual a la variable actual
vieja = actual
#iniciamos un ciclo para recorrer la lista items
for contador in range(0,30):
 creation_date = (f["items"][contador]["creation_date"]) 
#iniciamos una condicion, si creation_date es menor a la variable vieja entonces le asignamos creation_date a la variable vieja
 if creation_date<vieja:
   vieja=creation_date
#este contador sirve para obtener el nuemro de pregunta de la lista
   contador2=contador
print()
#imprimimos la fecha mas actual y la fecha mas vieja en el formato del archivo json y en un formato distinto para que sea mas legible
print("la respuesta mas actual es la:",contador1,"con la fecha",actual,"ó",datetime.datetime.fromtimestamp(int(actual)).strftime('%Y-%m-%d %H:%M:%S'))  
print("la respuesta mas vieja es la:",contador2,"con la fecha",vieja,"ó",datetime.datetime.fromtimestamp(int(vieja)).strftime('%Y-%m-%d %H:%M:%S'))