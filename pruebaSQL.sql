create database prueba;
use prueba;
CREATE TABLE aerolineas (
    id_aerolinea INT PRIMARY KEY,
    nombre_aerolinea VARCHAR(20)
);

CREATE TABLE aeropuertos (
    id_aeropuerto INT PRIMARY KEY,
    nombre_aerolinea VARCHAR(20)
);

CREATE TABLE movimientos (
    id_movimiento INT PRIMARY KEY,
    descripcion VARCHAR(20)
);

CREATE TABLE vuelos (
    id_aerolinea INT,
    id_aeropuerto INT,
    id_movimiento INT,
    dia DATE,FOREIGN KEY (id_aerolinea)
        REFERENCES aerolineas (id_aerolinea),
    FOREIGN KEY (id_aeropuerto)
        REFERENCES aeropuertos (id_aeropuerto),
    FOREIGN KEY (id_movimiento)
        REFERENCES movimientos (id_movimiento)
);

insert into aerolineas (id_aerolinea, nombre_aerolinea) values(1,"Volaris"),
															  (2,"Aeromar"),
															  (3,"Interjet"),
															  (4,"Aeromexico");
                                                              
insert into aeropuertos (id_aeropuerto, nombre_aerolinea) values(1,"Benito Juarez"),
															    (2,"Guanajuato"),
															    (3,"La paz"),
															    (4,"Oaxaca");

insert into movimientos (id_movimiento, descripcion) values(1,"Salida"),
														   (2,"Llegada");
                                                           
insert into vuelos (id_aerolinea, id_aeropuerto, id_movimiento, dia) values(1,1,1,'2021-05-02'),
																		   (2,1,1,'2021-05-02'),
																		   (3,2,2,'2021-05-02'),
																		   (4,3,2,'2021-05-02'),
																		   (1,3,2,'2021-05-02'),
																		   (2,1,1,'2021-05-02'),
																		   (2,3,1,'2021-05-04'),
																		   (3,4,1,'2021-05-04'),
																		   (3,4,1,'2021-05-04');
																												
/*1. ¿Cuál es el nombre aeropuerto que ha tenido mayor movimiento durante el año?*/                                                                
SELECT 
    a.nombre_aerolinea, COUNT(*) AS mayor_movimiento
FROM
    vuelos v
        JOIN
    aeropuertos a ON v.id_aeropuerto = a.id_aeropuerto
GROUP BY a.nombre_aerolinea
ORDER BY mayor_movimiento DESC
LIMIT 1;
                                                                
/*2. ¿Cuál es el nombre aerolínea que ha realizado mayor número de vuelos durante el año?*/          
SELECT 
    a.nombre_aerolinea, COUNT(*) AS mayor_numero_vuelos
FROM
    vuelos v
        JOIN
    aerolineas a ON v.id_aerolinea = a.id_aerolinea
GROUP BY a.nombre_aerolinea
ORDER BY mayor_numero_vuelos DESC
LIMIT 1;
                                                                
/*3. ¿En qué día se han tenido mayor número de vuelos?*/                                                                
SELECT 
    dia, COUNT(*) AS vuelos_en_el_dia
FROM
    vuelos
GROUP BY dia
ORDER BY vuelos_en_el_dia DESC
LIMIT 1;
                                                                
/*4. ¿Cuáles son las aerolíneas que tienen mas de 2 vuelos por día?*/
SELECT 
    v.dia,
    a.nombre_aerolinea,
    v.id_aerolinea,
    COUNT(*) AS mas_de2_xdia
FROM
    vuelos v
        JOIN
    aerolineas a ON v.id_aerolinea = a.id_aerolinea
GROUP BY dia , v.id_aerolinea
HAVING mas_de2_xdia > 2;             													